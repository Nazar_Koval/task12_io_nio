package com.koval.view;

import com.koval.buffer.*;
import com.koval.droidship.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    private static Scanner scanner = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(View.class);
    private Map<String, String> options;

    public View(){
        options = new LinkedHashMap<>();
        options.put("1", "1 - Serialize a ship with droids to file droids.txt");
        options.put("2", "2 - Deserialize a ship with droids from file droids.txt");
        options.put("3", "3 - Read from file using buffer");
        options.put("4", "4 - Read from file without buffer");
        options.put("5", "5 - PushBackStream test");
        options.put("6", "6 - Get all the comments from Comment class");
        options.put("7", "7 - Channel test");
        options.put("8", "8 - Quit");
        options.put("dir", "dir - directory task");
    }

    enum Options implements Executable{

        FIRST{
            @Override
            public void execute() {
                Ship ship = new Ship();
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(
                            new FileOutputStream("E:\\task12\\src\\main\\java\\com\\koval\\droidship\\droids.txt"));
                    oos.writeObject(ship.toString());
                    oos.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        },
        SECOND{
            @Override
            public void execute() {
                String from = null;

                try {
                    ObjectInputStream ois = new ObjectInputStream(
                            new FileInputStream("E:\\task12\\src\\main\\java\\com\\koval\\droidship\\droids.txt"));
                    from = (String) ois.readObject();
                }catch (IOException | ClassNotFoundException e){
                    e.printStackTrace();
                }

                assert from != null;
                logger.trace(from+ "\n");
                logger.trace("\nDone!\n");
            }
        },
        THIRD{
            @Override
            public void execute() {
                int count = 0;
                long start = System.currentTimeMillis();

                try {

                    FileReader fr = new FileReader("E:\\task12\\src\\main\\java\\fileToRead.txt");

                    while (fr.read()!= -1){
                        count++;
                    }
                    fr.close();
                }catch (IOException e){
                    e.printStackTrace();
                }

                long finish = System.currentTimeMillis();
                logger.trace("Amount of characters read from file: " + count);
                logger.trace("\nTime: " + (finish-start) + "\n");
                logger.trace("\nDone!\n");
            }
        },
        FOURTH{
            @Override
            public void execute() {
                int count = 0;
                long start = System.currentTimeMillis();

                try {
                    BufferedReader bf = new BufferedReader(
                            new FileReader("E:\\task12\\src\\main\\java\\fileToRead.txt"));
                    while (bf.read() != -1){
                        count++;
                    }
                    bf.close();

                }catch (IOException e){
                    e.printStackTrace();
                }

                long finish = System.currentTimeMillis();
                logger.trace("Amount of characters read from file: " + count);
                logger.trace("\nTime in milliseconds: " + (finish-start));
                logger.trace("\nDone!\n");
            }
        },
        FIFTH{
            @Override
            public void execute()throws Exception {
                PrintWriter pw = new PrintWriter(System.out, true);
                String str = "Dynamic programming is both a mathematical optimization method and a computer programming method.";
                byte[] bytes = str.getBytes();
                ByteArrayInputStream bai = new ByteArrayInputStream(bytes);
                PushbackInputStream pbi = new PushbackInputStream(bai);
                int c;

                while ((c = pbi.read()) != -1) {
                    pw.print((char)c);
                }
                pw.println();

                int numberOfBytes = pbi.read(bytes,0,19);

                for (int i = 0; i < 19; i++) {
                    pw.print((char)bytes[i]);
                }
                pw.println();

                pbi.close();
                bai.close();

                logger.trace("\nDone!\n");
            }
        },
        SIXTH{
            @Override
            public void execute() throws Exception {
                BufferedReader bf = new BufferedReader(
                        new FileReader("E:\\task12\\src\\main\\java\\com\\koval\\comment\\Comment.java"));
                int c;
                int tmp = 0;

                while ((c = bf.read()) != -1){

                    if((char)c == '/' && (char)tmp == '/'){
                        int c1;

                        while ((c1 = bf.read()) != -1){

                            if(Character.isLetter((char)c1) || (char)c1 == ' '){
                                logger.trace((char)c1);
                            }

                            if((char)c1 == '\n')break;
                        }
                        logger.trace("\n");
                    }
                    else if((char)c == '*' && (char)tmp == '/'){
                        int c1;

                        while ((c1 = bf.read()) != -1){

                            if(Character.isLetter((char)c1) || (char)c1 == ' ' || (char)c1 == '\n'){
                                logger.trace((char)c1);
                            }

                            if((char)c1 == '*')break;
                        }
                        logger.trace("\n");
                    }
                    tmp = bf.read();
                }
                logger.trace("\nDone!\n");
            }
        },
        SEVENTH{
            @Override
            public void execute() throws Exception {
                SomeBuffer someBuffer = new SomeBuffer();
                String key;
                logger.trace("1 - read from channel\n" +
                        "2 - write to channel\n");
                logger.trace("Input option: ");
                key = scanner.nextLine();

                if(key.equals("1")) someBuffer.readFromChannel();

                else if(key.equals("2")) someBuffer.writeToChannel();
            }
        },
        EXIT{
            @Override
            public void execute() {
                logger.trace("Bye!");
                System.exit(0);
            }
        }
    }

    private void showDirectory(File file){
        logger.trace("Dir: " + file.getName() + "\n");
        File[]files = file.listFiles();

        if (files != null) {

            for (File f:files) {

                if(f.isDirectory()){
                    showDirectory(f);
                }
                else {
                    logger.trace("File: " + f.getName() + "\n");
                }
            }
        }
    }

    private void show(){
        logger.trace("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
        for (String s:options.values()) {
            logger.trace(s + "\n");
        }
        logger.trace("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
        logger.trace("---> ");
    }

    public void interact()throws Exception {
        String key;

        while (true) {
            show();
            key = scanner.nextLine();

            if (key.length() > 3) logger.trace("Wrong option input!");

            else {

                if (key.equals("dir")) {
                    File file = new File("E:\\");
                    showDirectory(file);
                } else {
                    int parsed = Integer.parseInt(key);

                    if (parsed <= 0 || parsed > 8) logger.trace("Wrong option input!");

                    else Options.values()[parsed - 1].execute();
                }
            }
        }
    }
}
