package com.koval.droidship;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Random;

public class Ship implements Serializable {

    private Droid[]droids;
    private int capacity;

    public Ship(){
        generateDroids();
    }

    private void generateDroids(){
        Random rd = new Random();
        capacity = rd.nextInt(33);
        droids = new Droid[capacity];
        for (int i = 0; i < capacity; i++) {
            droids[i] = new Droid(rd.nextInt(1000),rd.nextInt(101),
                    rd.nextInt(101), rd.nextInt(1000));
        }
    }

    @Override
    public String toString() {
        return "\nShip{" +
                "droids=" + Arrays.toString(droids) + "\n"+
                "capacity=" + capacity +
                '}';
    }
}
