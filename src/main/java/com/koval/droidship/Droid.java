package com.koval.droidship;

import java.io.Serializable;

public class Droid implements Serializable {

    private transient int ID;
    private int strength;
    private int accuracy;
    private int range;

    public Droid(int ID, int strength, int accuracy, int range){
        this.ID = ID;
        this.strength = strength;
        this.accuracy = accuracy;
        this.range = range;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    @Override
    public String toString() {
        return "\nDroid{" +
                "ID=" + ID + "\n"+
                "strength=" + strength + "\n"+
                "accuracy=" + accuracy + "\n"+
                "range=" + range +
                '}';
    }
}
