package com.koval.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import org.apache.logging.log4j.*;

public class Server {

    private static Logger logger = LogManager.getLogger(Server.class);

    @SuppressWarnings("Duplicates")
    public static void main(String[] args) throws Exception {

        ServerSocket ss = new ServerSocket(888);
        Socket s = ss.accept();
        logger.trace("Connection established\n");

        PrintStream ps = new PrintStream(s.getOutputStream());
        BufferedReader fromClient = new BufferedReader(
                new InputStreamReader(s.getInputStream()));
        BufferedReader answer = new BufferedReader(
                new InputStreamReader(System.in));

        while (true) {
            String str, str1;

            while (!(str = fromClient.readLine()).contains("exit")) {
                logger.trace("Client`s request: ");
                logger.trace(str + "\n");
                str1 = answer.readLine();
                ps.print("Server answers: ");
                ps.println(str1);
            }

            ps.close();
            fromClient.close();
            answer.close();
            ss.close();
            s.close();

            System.exit(0);
        }
    }
}
