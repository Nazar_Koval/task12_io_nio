package com.koval.buffer;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

public class SomeBuffer  {

    public FileChannel fileChannel;
    public RandomAccessFile accessFile;

    public SomeBuffer()throws Exception{
        accessFile = new RandomAccessFile("E:\\task12\\src\\main\\java\\com\\koval\\droidship\\droids.txt","rw");
        fileChannel = accessFile.getChannel();
    }

    public void close()throws IOException {
        fileChannel.close();
    }

    public void readFromChannel()throws Exception{
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        fileChannel.read(buffer);
        String from = new String(buffer.array(), StandardCharsets.UTF_8);
        System.out.println("Data read from channel: \n");
        System.out.println(from);
    }

    public void writeToChannel()throws Exception{
        ByteBuffer rBuffer = ByteBuffer.allocate(1024);
        System.out.println("File channel before writing: \n");
        fileChannel.read(rBuffer);
        String read_before = new String(rBuffer.array(),StandardCharsets.UTF_8);
        System.out.println(read_before);
        String addData = "###############";
        System.out.println("Data to write: " + addData);
        ByteBuffer wBuffer = ByteBuffer.wrap(addData.getBytes(StandardCharsets.UTF_8));
        fileChannel.write(wBuffer,1025);
    }

    public void afterWriting()throws Exception{
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        System.out.println("\nFile channel after writing: \n");
        fileChannel.read(buffer);
        String read = new String(buffer.array(),StandardCharsets.UTF_8);
        System.out.println(read);
    }
}
