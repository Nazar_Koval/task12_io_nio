package com.koval.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import org.apache.logging.log4j.*;

public class Client {

    private static Logger logger = LogManager.getLogger(Client.class);

    @SuppressWarnings("Duplicates")
    public static void main(String[] args) throws Exception {

        Socket s = new Socket("localhost", 888);
        PrintStream ps =
                new PrintStream(s.getOutputStream());
        BufferedReader fromServer = new BufferedReader(
                new InputStreamReader(s.getInputStream()));
        BufferedReader request = new BufferedReader(
                new InputStreamReader(System.in));

        String str, str1;
        logger.trace("Connection established\n");

        while (!(str = request.readLine()).contains("exit")) {
            ps.print(str + "\n");
            str1 = fromServer.readLine();
            logger.trace(str1 + "\n");
        }

        ps.close();
        fromServer.close();
        request.close();
        s.close();
    }
}
